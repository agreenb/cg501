import { ChannelItem } from './channel-item';

export const CHANNEL_DATA: ChannelItem[] = [
  {position: 2, name: 'SiriusXM Hits 1', logo: 'SiriusXmHits.png', description: 'Pop | Today\'s Pop Hits'},
  {position: 3, name: 'Venus', logo: 'Venus.png', description: 'Pop | Pop Music You Can Move To'},
  {position: 4, name: 'Pitbull\'s Globalization', logo: 'PitBullGlobalization-Logo-76x76-P-042115.png', description: 'Pop | Worldwide Rhythmic Hits'},
  {position: 5, name: '\'50s on 5', logo: '50son5.png', description: 'Pop | \'50s Pop Hits'},
  {position: 6, name: '\'60s on 6', logo: '60son6_Logo_P_76X76_101310.png', description: 'Pop | \'60s Pop Hits w/ Cousin Brucie'},
  {position: 7, name: '\'70s on 7', logo: '70son7-P-76x76-063015.png', description: 'Pop | \'70s Pop Hits'},
  {position: 8, name: '\'80s on 8', logo: '80son8_Logo_P_76X76_101310.png', description: 'Pop | \'80s Pop Hits'},
  {position: 9, name: '\'90s on 9', logo: '90son9_Logo_P_76X76_101310.png', description: 'Pop | \'90s Pop Hits'},
  {position: 10, name: 'Pop2K', logo: 'Pop2K_Logo_P_76X76_101310.png', description: 'F'},
  {position: 13, name: 'Velvet', logo: 'Velvet-9361-X-Logo-76x76-P-102513.png', description: 'Ne'},
];