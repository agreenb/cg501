export interface ChannelItem {
  name: string;
  position: number;
  logo?: string;
  description: string;
}