import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { OverlayModule } from '@angular/cdk/overlay';
import { HttpClientModule } from '@angular/common/http';

import 'hammerjs';
import { AppMaterialModule } from './app-material.module';
import { ToolbarModule } from './toolbar/toolbar.module';
import { CoreModule } from './core/core.module';

import { AppComponent } from './app.component';
import { NavBarComponent } from './components/nav-bar/nav-bar.component';
import { OptionItemsComponent } from './components/nav-bar/option-items/option-items.component';
import { TabsBarComponent } from './components/nav-bar/tabs/tabs-bar.component';
import { MainTabsComponent } from './components/nav-bar/tabs/main-tabs.component';
import { SubTabsComponent } from './components/nav-bar/tabs/sub-tabs.component';
import { SearchFieldComponent } from './components/nav-bar/tabs/search-field.component';
import { ChannelTableComponent } from './components/channel-table/channel-table.component';
import { TileComponent } from './tile/tile.component'; 


const appRoutes: Routes = [


  { path: 'music',      component: SubTabsComponent },
  { path: 'sports',      component: SubTabsComponent },
  { path: 'news',      component: SubTabsComponent },
  { path: 'howard',      component: SubTabsComponent },
  { path: 'talk',      component: SubTabsComponent },
  { path: 'all',      component: SubTabsComponent },

  { path: '',
    redirectTo: '/all',
    pathMatch: 'full'
  },
];

@NgModule({
  declarations: [
    AppComponent,
    NavBarComponent,
    OptionItemsComponent,
    TabsBarComponent,
    MainTabsComponent,
    SubTabsComponent,
    SearchFieldComponent,
    ChannelTableComponent,
    TileComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppMaterialModule,
    FormsModule,
    OverlayModule,
    ReactiveFormsModule,
    HttpClientModule,
    CoreModule,
    ToolbarModule,
     RouterModule.forRoot(
      appRoutes,
      { enableTracing: true } // <-- debugging purposes only
    )
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
