import { Component, OnInit } from '@angular/core';
import {DomSanitizer} from '@angular/platform-browser';
import {MatIconRegistry} from '@angular/material';

@Component({
  selector: 'option-items',
  templateUrl: './option-items.component.html',
  styleUrls: ['./option-items.component.css']
})
export class OptionItemsComponent implements OnInit {
	pkgselected = "allaccess"

  constructor(iconRegistry: MatIconRegistry, sanitizer: DomSanitizer) {
    iconRegistry.addSvgIcon(
        'print-icon',
        sanitizer.bypassSecurityTrustResourceUrl('assets/img/icons/print-icon.svg'));
    iconRegistry.addSvgIcon(
        'download-icon',
        sanitizer.bypassSecurityTrustResourceUrl('assets/img/icons/download-icon.svg'));
    
  }

  ngOnInit() {
  }

}

