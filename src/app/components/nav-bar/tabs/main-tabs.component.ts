import { Component, OnInit } from '@angular/core';
import {DomSanitizer} from '@angular/platform-browser';
import {MatIconRegistry} from '@angular/material';

@Component({
  selector: 'main-tabs',
  template: `

  <div class="row">
	
		<div class="left">
			<div class="tabs">
				<a *ngFor="let section of sections" routerLink="/{{section.key}}" routerLinkActive="active">
					<div class="main-tab underline" >{{section.name}}</div>
				</a>
				<div class="mat-ink-bar" style="visibility: visible; left: 0px; width: 160px;"></div>
			</div>
		</div>
		
		<div class="right">
			<search-field id="search-field" class="main-tab"></search-field>
		</div>
	</div>
	<router-outlet></router-outlet>`,
  styleUrls: ['./tabs-bar.component.scss']
})
export class MainTabsComponent implements OnInit {

		   sections: any = [
   		 {
				name : "Music",
				key: "music",
				genres: ["All Music", "Rock", "Pop", "Country", "R&amp;B / Hip-Hop", "Christian", "Jazz/Standards", "Classical", "Dance/Electronic", "Latino", "Holiday", "Canadian" ]
			},{
				name : "Sports",
				key: "sports",
				genres: ["Sports", "NFL Play-by-Play", "MLB Play-by-Play", "College Play-by-Play", "NBA Play-by-Play", "NHL Play-by-Play", "Sports Play-by-Play"]
			},{
				name : "Talk",
				key: "talk",
				genres: ["All Talk", "Entertainment", "Comedy", "Religion", "Kids", "More"]
			},{
				name : "News",
				key: "news",
				genres: ["All News", "News/Public Radio", "Politics/Issues", "Traffic/Weather"]
			},{
				name : "Howard Stern",
				key: "howard",
				genres: ["Howard Stern"]
			},{
				name : "All",
				key: "all",
				genres: ["Featured"]
			}
   ];


  constructor(iconRegistry: MatIconRegistry, sanitizer: DomSanitizer) {
    iconRegistry.addSvgIcon(
        'search-icon',
        sanitizer.bypassSecurityTrustResourceUrl('assets/img/icons/search-icon.svg'));
    
  }

  ngOnInit() {
  }

}
