import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'sub-tabs',
  template: `
  <div class="sub-row">
			<div class="container">
				<div class="sub-tabs">
					<mat-tab-group>
						  <mat-tab class="sub-section" *ngFor="let genre of genres" label="{{genre}}">
						  	<channel-table class="my-light-theme"></channel-table>
					  		
						  </mat-tab>

					</mat-tab-group>
				</div>
			</div>
		</div>`,
  styleUrls: ['./tabs-bar.component.scss']
})
export class SubTabsComponent implements OnInit {

		maintab = "Music"
	   genres : any = ["All Music", "Rock", "Pop", "Country", "R&B / Hip-Hop", "Christian", "Jazz/Standards", "Classical", "Dance/Electronic", "Latino", "Holiday", "Canadian" ]
  constructor() { }

  ngOnInit() {
  }

}
